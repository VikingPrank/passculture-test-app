import React, { useState } from "react";
import {
  Image,
  StyleSheet,
  Text,
  View,
  ScrollView,
  Picker
} from "react-native";
import { logo, cannes1 } from "./assets";
import Form from "./components/Form";
import Post from "./components/Post";
import { getUniqueCategories, getUniqueDates } from "./helpers/postHelper";
import { news } from "./news.json";

const styles = StyleSheet.create({
  header: {
    backgroundColor: "white",
    alignItems: "center",
    justifyContent: "center",
    color: "black"
  },
  logo: {
    height: "10vh",
    width: "100vw"
  },
  image: {
    height: "40vw",
    width: "100vw"
  },
  title: {
    fontSize: "calc(10px + 1vh)",
    color: "black",
    flex: 1
  }
});

function App() {
  const categories = getUniqueCategories(news);
  const dates = getUniqueDates(news);
  const [selectedCategory, setSelectedCategory] = useState("All");
  const [selectedDate, setSelectedDate] = useState("All");

  const filteredNews = news.filter((article) => {
    if (selectedCategory === "All" && selectedDate === "All") {
      return true;
    } else if (selectedCategory === "All") {
      return article.date === selectedDate;
    } else if (selectedDate === "All") {
      return article.category === selectedCategory;
    } else {
      return (
        article.category === selectedCategory && article.date === selectedDate
      );
    }
  });

  function handleFormSubmit(newNews) {
    console.log(newNews);
  }

  return (
    <ScrollView>
      <View style={styles.header}>
        <View style={styles.header}>
          <Image
            accessibilityLabel="passCulture logo"
            source={{ uri: logo }}
            resizeMode="contain"
            style={styles.logo}
          />
          <Text style={styles.title}>Tu as entre 15 et 18 ans ?</Text>
        </View>
        <View>
          <Image
            source={{ uri: cannes1 }}
            resizeMode="cover"
            style={styles.image}
          />
        </View>
      </View>

      <Form onSubmit={handleFormSubmit} />

      <View style={styles.dropdowns}>
        <Picker
          selectedValue={selectedCategory}
          style={styles.picker}
          onValueChange={(itemValue) => setSelectedCategory(itemValue)}
        >
          <Picker.Item label="All Categories" value="All" />
          {categories.map((category, index) => (
            <Picker.Item key={index} label={category} value={category} />
          ))}
        </Picker>
        <Picker
          selectedValue={selectedDate}
          style={styles.picker}
          onValueChange={(itemValue) => setSelectedDate(itemValue)}
        >
          <Picker.Item label="All Dates" value="All" />
          {dates.map((date, index) => (
            <Picker.Item key={index} label={date} value={date} />
          ))}
        </Picker>
      </View>
      <View>
        {filteredNews.map((article, index) => (
          <Post key={index} article={article} />
        ))}
      </View>
    </ScrollView>
  );
}

export default App;
