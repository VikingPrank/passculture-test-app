import React, { useState } from "react";
import { StyleSheet, View, TextInput, Button } from "react-native";

const styles = StyleSheet.create({
  formContainer: {
    paddingHorizontal: 16,
    paddingVertical: 8
  },
  formInput: {
    marginBottom: 8,
    padding: 8,
    borderWidth: 1,
    borderColor: "#ccc",
    borderRadius: 4
  },
  formButton: {
    padding: 8
  }
});

export default function Form({ onSubmit }) {
  const [newsTitle, setNewsTitle] = useState("");
  const [newsCategory, setNewsCategory] = useState("");
  const [newsText, setNewsText] = useState("");

  function handleSubmit() {
    const newNews = {
      title: newsTitle,
      category: newsCategory,
      date: new Date(),
      text: newsText,
      images: []
    };
    onSubmit(newNews);
  }

  return (
    <View style={styles.formContainer}>
      <TextInput
        style={styles.formInput}
        placeholder="Title"
        value={newsTitle}
        onChangeText={setNewsTitle}
      />
      <TextInput
        style={styles.formInput}
        placeholder="Category"
        value={newsCategory}
        onChangeText={setNewsCategory}
      />
      <TextInput
        style={styles.formInput}
        placeholder="Text"
        multiline
        value={newsText}
        onChangeText={setNewsText}
      />
      <Button
        title="Create News"
        onPress={handleSubmit}
        style={styles.formButton}
      />
    </View>
  );
}
