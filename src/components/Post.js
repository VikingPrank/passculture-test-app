import React from "react";
import { Text, View } from "react-native";
const Post = ({ article }) => {
  const { title, category, date, text, images } = article;

  return (
    <View
      style={{ borderBottomColor: "grey", borderWidth: 1, paddingBottom: 10 }}
    >
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-evenly",
          paddingTop: 20
        }}
      >
        <Text style={{ fontSize: 20, padding: 5 }}>{title}</Text>
        <Text style={{ fontSize: 20, padding: 5 }}>Category: {category}</Text>
      </View>

      <Text style={{ fontSize: 16, padding: 5 }}>Date: {date}</Text>
      <Text style={{ fontSize: 15, padding: 5 }}>{text}</Text>
      {images && images.length > 0 && (
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-evenly"
          }}
        >
          {images.map((image, index) => (
            <img
              key={index}
              src={image}
              alt={`${index + 1}`}
              style={{ width: "200px", height: "auto" }}
            />
          ))}
        </View>
      )}
    </View>
  );
};

export default Post;
