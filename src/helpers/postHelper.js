export function getUniqueCategories(data) {
  const categories = data.map((article) => article.category);
  return Array.from(new Set(categories));
}

export function getUniqueDates(data) {
  const dates = data.map((article) => article.date);
  return Array.from(new Set(dates));
}
